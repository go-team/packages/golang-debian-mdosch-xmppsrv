# Changelog

## [0.3.3] 20224-11-02
### Added
- Add host-meta2 look up.

## [0.3.2] 2024-08-20
###  Added
- Add constant including version.

## [0.3.1] 2024-08-18
- Create a new release as v0.3.0 missed the fix from v0.2.6.

## [0.3.0] 2024-08-18
- Add function to request IP addresses using the custom resolvers.

## [0.2.6] 2024-01-19
### Changed
- Don't return an error if either of the xmpp or xmpps look ups returns a result.

## [0.2.5] 2023-03-27
### Changed
- Don't use CNAMES of xmpp domain for xmpp SRV lookups.

## [0.2.4]
### Changed
- The changes are the same as in 0.2.3 but a new release is created as not all changes were in 0.2.3 due to a mistake in the release process.

## [0.2.3]
### Added
- Add CNAME lookup.

### Changed
- Check resolver IP for validity.
- Improve error strings.
- Always return the error if one of the lookups failed.

## [0.2.2]
### Changed
- Added error handling for wrong resolver and DoT configuration.

## [0.2.1]
### Changed
- Check that DoT server string split at `#` returns 2 values.

## [0.2.0]
### Added
- Support for custom DNS resolvers and DoT.

### Changed
- Don't capitalize error strings.
- Don't end error strings with punctuation.

## [0.1.1]
### Changed
- Don't sort SRV records with identical priority by weight.
- Fix a typo in error message when no server SRV records are found.

## [0.1.0]
### Added
- Initial release
