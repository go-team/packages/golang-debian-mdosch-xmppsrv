# xmppsrv

Lookup xmpp srv records

## usage

Import the library into your go project by adding `salsa.debian.org/mdosch/xmppsrv`
to your imports.


All functions return `[]SRV` with `SRV` being the following struct:

```
type SRV struct {
	Type     string
	Target   string
	Port     uint16
	Priority uint16
	Weight   uint16
}
```

Type is either `xmpp-client`, `xmpps-client`, `xmpp-server` or `xmpps-server`.


The functions `LookupXmppServer`, `LookupXmppsServer`, `LookupXmppClient` and
`LookupXmppsClient` are called with the server name and return the respective
SRV records.

The function `LookupClient` and `LookupServer` are also called with the server
name but return `xmpp` and `xmpps` SRV records ordered by priority and weight.

To configure a custom resolver or to use DoT all the lookup functions implement
an interface on the Config type:

```
type Config struct {
        Resolver string
        DoT      bool
}
```

Set Resolver to an IP address and DoT to false to use the DNS resolver on port 53 on the given IP address.
Set Resolver to an IP and domain name (e.g. "5.1.66.255#dot.ffmuc.net") and DoT to true to use DNSoverTLS (DoT) on that resolver.
